package com.example.daytodaytest.network

import com.example.daytodaytest.model.Hero
import retrofit2.http.GET
import retrofit2.Call


interface ApiUtils {

    @GET("marvel")
    fun getHeroes(): Call<List<Hero>>

}