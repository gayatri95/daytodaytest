package com.example.daytodaytest.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CallApi {

    private var retrofit: Retrofit? = null

    var BASE_URL = "https://simplifiedcoding.net/demos/"

    companion object{
        fun getInstance() : CallApi{
            return CallApi()
        }
    }

    fun getRetrofitInstance(): Retrofit? {
        if (retrofit == null) {
            retrofit = retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit
    }

}