package com.example.daytodaytest.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.daytodaytest.model.Hero

@Dao
interface HeroDao {

    @Insert
    fun insertHero(hero: List<Hero>?)

    @Query("DELETE FROM hero_table")
    fun deleteAll()

    @Query("SELECT * FROM hero_table")
    fun getAllHeroList(): LiveData<List<Hero>>
}