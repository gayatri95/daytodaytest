package com.example.daytodaytest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.daytodaytest.model.Hero

@Database(entities = [Hero::class],version = 1)
abstract class HeroDatabase: RoomDatabase() {

    abstract fun heroDao():HeroDao

    companion object{
        private var instance: HeroDatabase? = null

        fun getInstance(context: Context): HeroDatabase? {
            if (instance == null) {
                synchronized(HeroDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        HeroDatabase::class.java, "hero_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }

            }
            return instance
        }
   }
 }