package com.example.daytodaytest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.daytodaytest.R
import com.example.daytodaytest.model.Hero
import kotlinx.android.synthetic.main.hero_item.view.*


class HeroListAdapter() : RecyclerView.Adapter<HeroListAdapter.MyViewHolder>() {

    private var listHero: List<Hero>? = null
    private var listener: OnHeroItemClickListener? = null


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroListAdapter.MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.hero_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {

        return listHero?.size ?: 0
    }

    fun submitList(listHero: List<Hero>?) {
        this.listHero = listHero
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: HeroListAdapter.MyViewHolder, position: Int) {

        val hero = listHero?.get(position)

        Glide.with(holder.itemView.context)
            .load(hero?.imageurl)
            .into(holder.itemView.iv_hero)

        holder.itemView.tv_name.text = hero?.name
        holder.itemView.tv_team.text = hero?.team

        holder.itemView.setOnClickListener {
            if (position != RecyclerView.NO_POSITION) {
                if (hero != null) {
                    listener?.onItemClick(hero)
                }
            }
        }
    }


    interface OnHeroItemClickListener {
        fun onItemClick(hero: Hero)
    }

    fun setOnItemClickListener(listener: OnHeroItemClickListener) {
        this.listener = listener
    }

}