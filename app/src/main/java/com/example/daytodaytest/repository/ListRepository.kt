package com.example.daytodaytest.repository

import android.app.Application
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import com.example.daytodaytest.database.HeroDao
import com.example.daytodaytest.database.HeroDatabase
import com.example.daytodaytest.model.Hero
import com.example.daytodaytest.network.ApiUtils
import com.example.daytodaytest.network.CallApi
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

class ListRepository(application: Application) {

    private var apiUtils: ApiUtils? = null
    private lateinit var heroDao: HeroDao
    private var context = application

    init {
        apiUtils = CallApi.getInstance().getRetrofitInstance()?.create(ApiUtils::class.java)

        //for offline storage
        val database: HeroDatabase = HeroDatabase.getInstance(
            application.applicationContext
        )!!
        heroDao = database.heroDao()

        callAPIForListOfHero()
    }


    private fun callAPIForListOfHero() {

        apiUtils?.getHeroes()?.enqueue(object : retrofit2.Callback<List<Hero>> {
            override fun onFailure(call: Call<List<Hero>>, t: Throwable) {

                Log.e("ListRepo onFailure :", t.toString())

                if (t is IOException) {
                    // show No Connectivity message to user or do whatever you want.
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_LONG).show()
                }

            }

            override fun onResponse(call: Call<List<Hero>>, response: Response<List<Hero>>) {

                if (response.isSuccessful) {
                    if (!response.body().isNullOrEmpty()) {

                        Log.e("ListRepo onSuccess :", response.body().toString())

                        deleteList()
                        insertList(response.body())

                    }
                }
            }

        })
    }

    private fun insertList(list: List<Hero>?) {
        val insertHeroAsyncTask = InsertHeroAsyncTask(heroDao).execute(list)
    }

    private fun deleteList() {
        val insertHeroAsyncTask = DeleteHeroAsyncTask(heroDao).execute()
    }

    fun getAllHeroList(): LiveData<List<Hero>>? {
        return heroDao.getAllHeroList()
    }

    companion object {

        private class InsertHeroAsyncTask(val heroDao: HeroDao) : AsyncTask<List<Hero>, Unit, Unit>() {

            override fun doInBackground(vararg p0: List<Hero>?) {

                for (i in 0 until p0.size) {
                    heroDao.insertHero(p0[i])
                }
            }
        }

        private class DeleteHeroAsyncTask(val heroDao: HeroDao) : AsyncTask<Unit, Unit, Unit>() {

            override fun doInBackground(vararg p0: Unit?) {
                heroDao.deleteAll()
            }
        }


    }

}