package com.example.daytodaytest.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "hero_table")
class Hero(
    val name: String,
    val realname: String,
    val team: String,
    val firstappearance: String,
    val createdby: String,
    val publisher: String,
    val imageurl: String,
    val bio: String
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}