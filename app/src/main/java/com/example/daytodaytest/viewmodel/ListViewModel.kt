package com.example.daytodaytest.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.daytodaytest.model.Hero
import com.example.daytodaytest.repository.ListRepository

class ListViewModel(application: Application) : AndroidViewModel(application) {

    var listRepository: ListRepository? = null
    var listOfHeroo: LiveData<List<Hero>>? = null


    init {

        listRepository = ListRepository(application)
        listOfHeroo = listRepository!!.getAllHeroList()
    }

    fun getListOfHero(): LiveData<List<Hero>>? {
        return listOfHeroo
    }

}