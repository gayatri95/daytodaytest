package com.example.daytodaytest.view

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.daytodaytest.R
import com.example.daytodaytest.model.Hero
import kotlinx.android.synthetic.main.activity_hero_detail.*

class HeroDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hero_detail)

        val hero = intent.extras?.get("hero") as Hero

        Log.e("HeroDetailActivity : ", hero.toString())

        setDataToView(hero)

    }

    private fun setDataToView(hero: Hero) {
        Glide.with(this)
            .load(hero.imageurl)
            .into(iv_hero)

        tv_name?.text = hero.name
        tv_real_name?.text = hero.realname
        tv_publisher?.text = hero.publisher
        tv_bio?.text = hero.bio
        tv_created_by.text = hero.createdby
        tv_first_appearance.text = hero.firstappearance


    }

}
