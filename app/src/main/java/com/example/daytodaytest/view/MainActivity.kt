package com.example.daytodaytest.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.daytodaytest.R
import com.example.daytodaytest.adapter.HeroListAdapter
import com.example.daytodaytest.model.Hero
import com.example.daytodaytest.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {


    var originalList: List<Hero>? = null
    var searchList: List<Hero>? = null
    var listViewModel: ListViewModel? = null
    val adapter = HeroListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv_list?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_list?.setHasFixedSize(true)
        rv_list?.adapter = adapter


        listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        listViewModel?.getListOfHero()?.observe(this,
            Observer<List<Hero>> {
                originalList = it
                adapter.submitList(it)
                Log.e("Acitvity : ", it.toString())
            })


        sv_hero?.setOnQueryTextListener(this)

        adapter.setOnItemClickListener(object : HeroListAdapter.OnHeroItemClickListener {
            override fun onItemClick(hero: Hero) {

                val intent = Intent(this@MainActivity, HeroDetailActivity::class.java)
                intent.putExtra("hero", hero)
                startActivity(intent)
            }
        })
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {

        Log.e("onQueryTextChange :", newText)

        newText?.let { searchText ->
            searchList = originalList?.filter { hero ->
                hero.name.contains(searchText, true) || hero.team.contains(searchText, true)
            }
        }

        if (!searchList.isNullOrEmpty()) {
            adapter.submitList(searchList)
        } else {
            searchList = null
            adapter.submitList(originalList)
        }


        return false
    }

}
